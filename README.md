# SCCB Requests

This repo is where users of the LIGO Data Grid can submit requests for new or updated packages to be considered for deployment on the system.

## Making a request

To make a request for a new/updated package:

- [click here](//git.ligo.org/duncanmmacleod/sccb-requests/issues/new?issuable_template=Request) to open a new ticket
- complete the form to include the name, version, and source URL for your updated package
- wait for an associated merge request to be automatically created that will build binary distributions for your package
