#!/usr/bin/env python

"""Build distribution files for an SCCB request
"""

import argparse
import logging
import os.path
import platform

import git

from pysccb import (__version__ as pysccb_version,
                    build as pysccb_build,
                    install as pysccb_install,
                    test as pysccb_test)
from pysccb.git import get_changed_files
from pysccb.yaml import read_config
from pysccb.utils import download_file

__author__ = 'Duncan Macleod <duncan.macleod@ligo.org>'
__version__ = pysccb_version

logging.basicConfig(level=logging.DEBUG, datefmt="%Y-%m-%d %H:%M:%S",
                    format="[%(asctime)s] %(levelname)+7s | %(message)s")

BUILDERS = {
    'rpm': pysccb_build.build_binary_rpms,
    'deb': pysccb_build.build_debs,
    'pip': None,
}
INSTALLERS = {
    'rpm': pysccb_install.local_install_yum,
    'deb': pysccb_install.local_install_deb,
    'pip': pysccb_install.local_install_pip,
}


# -- utilities --------------

def default_build_type():
    plat = platform.platform()
    if 'redhat' in plat:
        return 'rpm'
    return 'pip'


def changed_projects():
    # determine git upstream
    repo = git.Repo()
    head = repo.rev_parse('HEAD')
    if head == repo.rev_parse('origin/master'):
        base = 'HEAD^'
    else:
        base = 'origin/master'

    # find list of changed project files
    projects = []
    for change in get_changed_files(base, 'HEAD'):
        dir_, name = os.path.split(change)
        if dir_ == 'packages':
            projects.append(os.path.splitext(name)[0])
    return projects


def build_project(meta, buildtype, test=True):
    name = meta['package']['name']
    logging.info('- Building {0}'.format(name))
    tarball = download_file(meta['package']['source'])

    build = BUILDERS[buildtype]
    install = INSTALLERS[buildtype]

    # build
    if build is None:  # pip doesn't build
        installtarget = [tarball]
    else:
        logging.debug('-- Building {1} [{0}]'.format(buildtype, name))
        installtarget = build(tarball)

    # install()
    logging.debug('-- Installing {0}'.format(name))
    install(*installtarget)

    if test:
        tests = meta['test']['commands']
        logging.debug('-- Testing {0}:'.format(name))
        for cmd in tests:
            pysccb_test.run_test(cmd)

    logging.info('- Build complete')


# -- parse command line -----

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument('project', nargs='*',
                    help='project to build, if not given, '
                         'will be guessed from git history')
parser.add_argument('-V', '--version', action='version', version=__version__,
                    help='show version number and exit')
parser.add_argument('-b', '--build-type', type=str.lower,
                    choices=list(BUILDERS.keys()),
                    default=default_build_type(),
                    help='build type, one of: %(choices)s')
parser.add_argument('-t', '--skip-test', action='store_true', default=False,
                    help='skip tests (default: %(default)s)')

args = parser.parse_args()
projects = args.project

if not projects:
    projects = changed_projects()

# -- build projects ---------

logging.info('Building {0} projects:\n{1}'.format(
    len(projects),
    '\n'.join(projects),
))

for project in projects:
     config = os.path.join('packages', '{0}.yaml'.format(project))
     meta = read_config(config)
     build_project(meta, args.build_type, test=not args.skip_test)

logging.info('All projects built successfully')
