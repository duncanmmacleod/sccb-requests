#### Details

- **Package name:** <!-- insert package name -->
- **Version:** <!-- insert updated package version -->
- **Source:** <!-- insert http(s) URL to tarball, must be publicly accessible -->

#### Description

<!-- Insert description of changes here,
     e.g. link to release notes, changelog, ...
-->

#### Distributions

Please (de)select the distributions you are requesting:

- [x] Scientific Linux 7
- [ ] Debian 8 (Jessie)
- [x] Debian 9 (Stretch)
- [ ] Debian 10 (Buster)

#### Verificiation

Please select all of the following that apply:

- [ ] this is a new package
- [ ] this is a backwards-compatible update
- [ ] this is a backwards-incompatible update [API/ABI changes]
